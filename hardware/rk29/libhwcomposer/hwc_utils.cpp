/*
 * Copyright (C) 2010 The Android Open Source Project
 * Copyright (C) 2012-2013, The Linux Foundation. All rights reserved.
 *
 * Not a Contribution, Apache license notifications and license are
 * retained for attribution purposes only.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cutils/properties.h>
#include <utils/Log.h>
#include <utils/Timers.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <sys/prctl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <poll.h>
#include <ui/PixelFormat.h>
#include <stdio.h>
#include "hwc.h"
#include "../libgralloc_ump/gralloc_priv.h"
#include "../libon2/vpu_global.h"
#include <hardware/hwcomposer.h>

#include "ion/ion.h"

#include <ion/ion.h>
#include <linux/ion.h>
#include <linux/omap_ion.h>
#include <sys/mman.h>
#include <sys/stat.h>
#define RK_FBIOSET_CONFIG_DONE			0x4628
#define RK_FBIOSET_YUV_ADDR				0x5002
#define RK_FBIOSET_OVERLAY_STATE     	0x5018
#define RK_FBIOSET_ENABLE				0x5019
#define RK_FBIOSET_HWC_ADDR	 0x4624
#define ION_SYSTEM_HEAP_ID 8
#define ION_CMA_HEAP_ID 2
#ifdef TARGET_RK32
#define RK_HDMI_PATH "/sys/class/display/display0.HDMI/mode"
#else
#define RK_HDMI_PATH "/sys/class/display/HDMI/mode"
#endif

void dump_fps(void) {
	
	char property[PROPERTY_VALUE_MAX];
	if (property_get("debug.hwc.logfps", property, "0") && atoi(property) > 0) {
		static int mFrameCount;
	    static int mLastFrameCount = 0;
	    static nsecs_t mLastFpsTime = 0;
	    static float mFps = 0;
	    mFrameCount++;
	    nsecs_t now = systemTime();
	    nsecs_t diff = now - mLastFpsTime;
	    if (diff > ms2ns(500)) {
	        mFps =  ((mFrameCount - mLastFrameCount) * float(s2ns(1))) / diff;
	        mLastFpsTime = now;
	        mLastFrameCount = mFrameCount;
	        ALOGD("---mFps = %2.3f", mFps);
	    }
	    // XXX: mFPS has the value we want
	}
}

void dump_win_par(rk_fb_win_par *win_par,int fd,int area_no)
{
   ALOGD_IF(0,"using (fd=%d),"
	          "winid  = %d,"
              "alpha_mode = %d,"
              "g_alpha_val = %x,"
              "z_order = %d,"
              "format=%x,"
              "ion_fd = %d,"
              "phy_addr = 0x%x,"
              "acq_fence_fd = %d,\n"
              "x_offset = %d,"
              "y_offset = %d,"
              "xpos =%d ,"
              "ypos =%d ,"
              "xsize =%d ,"
              "ysize =%d ,"
              "xact =%d,"
              "yact =%d,"
              "xvir =%d,"
              "yvir=%d",
              fd,
              win_par->win_id,
              win_par->alpha_mode,
              win_par->g_alpha_val,
              win_par->z_order,
              win_par->data_format,
              win_par->area_par[area_no].ion_fd ,         
              win_par->area_par[area_no].phy_addr,
              win_par->area_par[area_no].acq_fence_fd,
              win_par->area_par[area_no].x_offset,
              win_par->area_par[area_no].y_offset,
              win_par->area_par[area_no].xpos,
              win_par->area_par[area_no].ypos,
              win_par->area_par[area_no].xsize,
              win_par->area_par[area_no].ysize,
              win_par->area_par[area_no].xact,
              win_par->area_par[area_no].yact,
              win_par->area_par[area_no].xvir,
              win_par->area_par[area_no].yvir
              
           );
   

}

void hwc_dump_layer(hwc_display_contents_1_t* list)
{
    size_t i;
    static int DumpSurfaceCount = 0;
    char pro_value[PROPERTY_VALUE_MAX];
    property_get("sys.dump",pro_value,0);
    //LOGI(" sys.dump value :%s",pro_value);
    if(!strcmp(pro_value,"true"))//!strcmp(pro_value,"true") && 
    {
        for (i = 0; list && (i < (list->numHwLayers - 1)); i++)
        {
            hwc_layer_1_t const * l = &list->hwLayers[i];
			if(l->flags & HWC_SKIP_LAYER)
			{
				ALOGI("layer %p skipped", l);
			}
			else
            {
                struct private_handle_t * handle_pre = (struct private_handle_t *) l->handle;
                int32_t SrcStride ;
                FILE * pfile = NULL;
                char layername[100] ;

                if( handle_pre == NULL)
                    continue;

                SrcStride = android::bytesPerPixel(handle_pre->format);
                memset(layername,0,sizeof(layername));
                system("mkdir /data/dump/ && chmod /data/dump/ 777 ");
                //mkdir( "/data/dump/",777);
                sprintf(layername,"/data/dump/dmlayer%d_%d_%d_%d.bin",DumpSurfaceCount,handle_pre->stride,handle_pre->height,SrcStride);
                DumpSurfaceCount ++;
                pfile = fopen(layername,"wb");
                if(pfile)
                {
                    fwrite((const void *)handle_pre->base,(size_t)(SrcStride * handle_pre->stride*handle_pre->height),1,pfile);
                    fclose(pfile);
                    ALOGI(" dump surface layername %s,w:%d,h:%d,formatsize :%d",layername,handle_pre->width,handle_pre->height,SrcStride);
                }
            }
        }

    }
    property_set("sys.dump","false");
}
int getCurrScreenInfo(int *xres, int *yres) {
    
        char type[50];
        char filename[] = "/sys/class/graphics/fb0/screen_info";      
        FILE *fp;      
        char strLine[50];                
        if((fp = fopen(filename,"r")) == NULL)      
        {          
            printf("error!");          
            return -1;      
        }      
        while (!feof(fp))     
        {        
            memset(type, 0, 50);
            memset(strLine, 0, 50);
            fgets(strLine, 50,fp);          
            if (strstr(strLine,"xres:"))
            {
               strcpy(type,strLine+5);
               *xres = atoi(type);
            } 
            else if (strstr(strLine,"yres:"))
            {
               strcpy(type,strLine+5);
               *yres = atoi(type);
            }
        }     
        ALOGV("current screen info:%d,%d",*xres,*yres);
        fclose(fp);                    
        return 0; 
}
#define FB0_WIN1_FB1_WIN0_FB2_WIN2 "201"
static void hwc_fb_swap(void)
{
	int fd;
	
	fd = open("/sys/class/graphics/fb0/map", O_RDWR, 0);
	if (fd < 0) {
		ALOGD("%s open file error", __func__);
		return;
	}
	
	write(fd, FB0_WIN1_FB1_WIN0_FB2_WIN2, 3);
	close(fd);
}

int hwc_open_cursor(hwc_context_t *ctx)
{
       // open hwc cursor
    char* buff_test = NULL;
	size_t t_len = 128*128*4;
	int mFbHandle = 0;
	if (!mFbHandle)
		mFbHandle = open("/dev/graphics/fb2", O_RDWR, 0);
	int ion_client = ion_open();
	int share_fd;
	int err = ion_alloc_fd(ion_client, t_len, 0, ION_HEAP_SYSTEM_MASK, 0,&share_fd);
	int fd = -1;
	struct stat st;
	uint8_t* filebuf;
	char* filename = "/system/usr/pointer_arrow.raw";
	if (stat(filename, &st) != 0) {
		ALOGD("\033[0;31m cannot stat file %s errno = %s\033[0m\n", filename,strerror(errno));
	}
	int filesize = st.st_size;
	filebuf = malloc(filesize);
	fd = open(filename, O_RDONLY);
	if (read(fd, filebuf, st.st_size) != st.st_size) {
		ALOGD("\033[0;31m read file error err = %s \033[0m\n",strerror(errno));
	}
#ifdef TARGET_RK32
	buff_test = mmap(NULL, t_len, PROT_READ|PROT_WRITE,MAP_SHARED, share_fd, 0);	
	memcpy(buff_test,filebuf,filesize);
	close(fd);
#else	
	/*****for bpp*****/
	int bpp[256] = {0}; 
	int numbpp = filesize/4;
	uint8_t lct[numbpp];
	bpp[0] = 0;
	int offset = 0;
	int pos = 0;
	int fileLen = filesize; 
	uint8_t *p = filebuf;
	while (fileLen > 0) {
		int data = *(int*)p; p+= sizeof(int);
		if (data == 0) {
			lct[offset/4] = data;
		} else {
			int i = 1;
			while (i<256 && bpp[i] != data && (bpp[i] != 0))
				i++;					
			if (bpp[i] == data) {
				if (data == 0x00ffffff)
					lct[offset/4] = 0;
				else
					lct[offset/4] = i;
			} else {
				bpp[i] = data;
				lct[offset/4] = i;
				if(data == 0x00ffffff)
					lct[offset/4] = 0;
			}
		}
		offset += 4;
		fileLen = filesize - offset;
	}
	/*****for init lut
	int temp;
	for (int i = 0; i < 256; i++) {
		temp = i;
		bpp[i] = temp + (temp << 8) + (temp << 16);
	}
	*******/
	memcpy(ctx->bpp,bpp,sizeof(int)*256);
	buff_test = mmap(NULL, t_len, PROT_READ|PROT_WRITE,MAP_SHARED, share_fd, 0);	
	memcpy(buff_test,lct,numbpp);
	close(fd);
	
	char* tmp = NULL;
	int tmpSize = 256*9;
	tmp = malloc(tmpSize);
	memset(tmp,0,tmpSize);
	for(int i=0;i<256;i++){
		char test[9];
		sprintf(test,"%08x",bpp[i]);
		strcat(tmp,test);
		if(i!=255)
			strcat(tmp," ");
	}
	int mret = -1;
	mret = open("/sys/class/graphics/fb0/hwc_lut",O_RDWR);
	write(mret,tmp,strlen(tmp));
	close(mret);
	if(tmp != NULL){
		free(tmp);
		tmp = NULL;
	}
#endif
	if(filebuf != NULL)
		free(filebuf);
	
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[2].fd = mFbHandle;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[2].active = true;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[2].ion_fd= share_fd;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[2].first_set = false;
	hwc_enable_layer(ctx, HWC_DISPLAY_PRIMARY, 2, 0);
	return 0;
	
}

int openFramebufferDevice(hwc_context_t *ctx)
{
	struct fb_fix_screeninfo finfo;
	struct fb_var_screeninfo info;
	
	char boot_noblank[PROPERTY_VALUE_MAX];
	property_get("sys.hwc.bootanim.noblank",boot_noblank,"0");
	
	//if(0 != strcmp(boot_noblank,"true"))
	//	hwc_fb_swap();		

#if 1
	if (hw_get_module(GRALLOC_HARDWARE_MODULE_ID,
		(const struct hw_module_t **)&ctx->gralloc_module)) {
		ALOGE("failed to get gralloc hw module");
		 return  -EINVAL;
	}
#endif


	int fb_fd = open("/dev/graphics/fb0", O_RDWR, 0);
	if(fb_fd < 0) {
		ALOGE("/dev/graphics/fb0 cat not access.");
		return -1;
	}

	if (int(info.width) <= 0 || int(info.height) <= 0) {
		// the driver doesn't return that information
		// default to 160 dpi
		info.width  = ((info.xres * 25.4f)/160.0f + 0.5f);
		info.height = ((info.yres * 25.4f)/160.0f + 0.5f);
	}

	float xdpi = (info.xres * 25.4f) / info.width;
	float ydpi = (info.yres * 25.4f) / info.height;
	//if(0 != strcmp(boot_noblank,"true")){	
		if (ioctl(fb_fd, FBIOGET_FSCREENINFO, &finfo) == -1)
			return -errno;
		
		if (finfo.smem_len <= 0)
			return -errno;
	//}
	info.yoffset = info.yres;

	//ioctl(fb_fd, FBIOPUT_VSCREENINFO, &info);
	
	///////////////////////////////////////////////////
	
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].enabledisplay = false;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd = fb_fd;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].active = true;
	//xres, yres may not be 32 aligned
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].stride = ctx->gralloc_module->info.xres_virtual * 4;
	/*
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].xres = info.xres;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].yres = info.yres;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].xdpi = xdpi;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].ydpi = ydpi;
	*/
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].vsync_period = 1000000000l / 60;
	
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].xres = ctx->gralloc_module->info.xres;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].yres = ctx->gralloc_module->info.yres;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].xdpi = ctx->gralloc_module->xdpi;  //xdpi;  ///ctx->gralloc_module->xdpi;
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].ydpi = ctx->gralloc_module->ydpi;  //ydpi;  ///ctx->gralloc_module->ydpi;
	
	ctx->dpyAttr[HWC_DISPLAY_PRIMARY].isActive = true;
	ctx->mCopyBit = new CopyBit();
#if 1
	//Enable overlay mode
	char property[PROPERTY_VALUE_MAX];
	int overlay;
	if (property_get("video.use.overlay", property, "0") && atoi(property) > 0) {
		ctx->overlaymode = atoi(property);
		fb_fd = open("/dev/graphics/fb1", O_RDWR, 0);
		if(fb_fd < 0) {
			ALOGE("/dev/graphics/fb0 cat not access.");
			return -1;
		}
		else
			ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[1].fd = fb_fd;
		
		hwc_enable_layer(ctx, HWC_DISPLAY_PRIMARY, 1, 0);
		
		if(atoi(property) == 1) {
		//	overlay = 1;
		//	ioctl(ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd, RK_FBIOSET_OVERLAY_STATE, &overlay);
			// If sys.ui.fakesize is not defined, default set to 1280x720
			if(property_get("sys.ui.fakesize", property, NULL) <= 0) {
				ALOGD("set default fake ui size 1280x720");
				property_set("sys.ui.fakesize", "1280x720");
			}
		}
		property_set("sys.hwc.compose_policy", "0");
	}
	else {
		property_set("sys.yuv.rgb.format", "1");
		overlay = 0;
		ioctl(ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd, RK_FBIOSET_OVERLAY_STATE, &overlay);
	}
	
//	if (ioctl(ctx->dpyAttr[HWC_DISPLAY_PRIMARY].layer[0].fd, RK_FBIOSET_CONFIG_DONE, &info) == -1)
//		return -errno;
#endif
	
	//hwc_open_cursor(ctx);

	return 0;
}

static unsigned int videodata[2];

void hwc_set_hdmi_mode(hwc_context_t *ctx, int NewFrameRate)
{
	int msize = 64;
	char szLine[msize];
	int fps = 0;
	char* temp = NULL;
	char test[msize];
	char w[16],h[16],hz[16];
	char curw[16],curh[16],curhz[16];
	FILE* fp0 = NULL;
	int mret = -1;
	
	char property[PROPERTY_VALUE_MAX];
	property_get("persist.sys.hwc.audohdmimode", property, 0);
	//ALOGD("\033[0;31m property = %d \033[0m\n",atoi(property));;
	if(atoi(property) == 0)
		return;
	
	if((fp0 = fopen(RK_HDMI_PATH,"r")) == NULL)
			return;
	
	fgets(szLine,sizeof(szLine),fp0);
		sscanf(szLine,"%[^a-zA-Z]%*[a-zA-Z]%[^a-zA-Z]%*[a-z]-%[0-9]",curw,curh,curhz);
	//ALOGD("curw = %s curh = %s curhz = %s",curw,curh,curhz);
	fclose(fp0);
	
	if(ctx->isModechange && NewFrameRate == 0)
	{
		// Restore original hdmi mode
		ctx->isModechange = false;
		mret = open(RK_HDMI_PATH,O_RDWR);
		if(strlen(ctx->mCurmode))
			write(mret,ctx->mCurmode,sizeof(ctx->mCurmode));
		close(mret);
		return;
	}	
	
	if(NewFrameRate != atoi(curhz))
	{
		// Set HDMI frame rate equal to current video frame rate
		FILE* fp = NULL;
#ifdef TARGET_RK32
		if((fp = fopen("/sys/class/display/display0.HDMI/modes","r")) == NULL)
			return;
#else
		if((fp = fopen("/sys/class/display/HDMI/modes","r")) == NULL)
			return;
#endif
		while(fgets(szLine,sizeof(szLine),fp) != NULL)
		{
			sscanf(szLine,"%[^a-zA-Z]%*[a-zA-Z]%[^a-zA-Z]%*[a-z]-%[0-9]",w,h,hz);
			if((atoi(w) == atoi(curw)) && (atoi(h) == atoi(curh)) && (atoi(hz)==NewFrameRate))
			{
				ctx->isModechange =  true;
				ALOGD("(atoi(w) == atoi(curw)) strlen szline = %d DecodeFrmNum =  %d",strlen(szLine),NewFrameRate);
				mret = open(RK_HDMI_PATH,O_RDWR);
				write(mret,szLine,sizeof(szLine));
				close(mret);
			}
		}
		fclose(fp);
	}
}

static void hwc_save_hdmi_mode(hwc_context_t *ctx)
{
	ctx->isModechange = false;
	FILE* fp0 = NULL;
	char szLine[32];
	fp0 = fopen(RK_HDMI_PATH,"r");
	fgets(szLine,sizeof(szLine),fp0);
	fclose(fp0);
	sprintf(ctx->mCurmode,"%s",szLine);
}

static unsigned int spritedata[1];

int hwc_sprite(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src)
{
	//ALOGD("hwc_sprite****************************");
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;
	struct tVPU_FRAME *pFrame  = NULL;	
	hwc_rect_t * DstRect = &(Src->displayFrame);
	int enable, nonstd, grayscale;
	
	ALOGD_IF(HWC_DEBUG, "%s format %x width %d height %d address 0x%x", __FUNCTION__, srchnd->format, srchnd->width, srchnd->height, srchnd->base);
	
	struct fb_var_screeninfo vinfo;    
	struct fb_fix_screeninfo finfo;

	unsigned int r,c,rowlen;   
	unsigned int bytespp,offset;   
	unsigned int xpos,ypos,format;
	
	
	if(ctx->dpyAttr[dpy].layer[2].fd < 0) {
		ALOGE("open fb fail");
	}else{
	
		if(ioctl(ctx->dpyAttr[dpy].layer[2].fd, FBIOGET_FSCREENINFO, &finfo) < 0) { 	   
			ALOGE("failed to get fix framebuffer info errno = %s\n",strerror(errno));		  
			return -1;    
		}
		
		if(ioctl(ctx->dpyAttr[dpy].layer[2].fd, FBIOGET_VSCREENINFO, &vinfo) < 0) { 	   
			ALOGE("failed to get var framebuffer info\n");		  
			return -1;
		}
	
		vinfo.reserved[0] = 0;
		vinfo.reserved[1] = 0;
		vinfo.reserved[2] = 0;
		vinfo.xoffset = 0;
		vinfo.yoffset = 0;
		vinfo.activate = FB_ACTIVATE_FORCE;
		vinfo.xres_virtual = vinfo.xres;
		vinfo.yres_virtual = vinfo.yres;
		vinfo.nonstd = 0;
		format = 1;
		vinfo.nonstd |= format;
		
		xpos = DstRect->left;
		ypos =  DstRect->top;
		
		vinfo.xres = 32;
		vinfo.yres = 32;
		int xsize = 32;
		int ysize = 32;
		vinfo.grayscale &= 0;
		vinfo.grayscale |= xsize << 8;
		vinfo.grayscale |= ysize << 20;//size
		vinfo.nonstd |= xpos << 8;//���?		vinfo.nonstd |= ypos << 20;
		/*
		ALOGD("buff_test = mmap ******* before");
		char * buff_test = NULL;
		size_t t_len = 128*128;
		buff_test = mmap(NULL, t_len, PROT_READ|PROT_WRITE,MAP_SHARED, ctx->dpyAttr[dpy].layer[2].ion_fd, 0);
		for(int i =0;i<128;i++)
			ALOGD("buff_test[%d] = %0x",i,buff_test[i]);
		ALOGD(" ioctl RK_FBIOSET_YUV_ADDR");
		*/
		if(ctx->dpyAttr[dpy].layer[2].first_set == false){
			ctx->dpyAttr[dpy].layer[2].first_set = true;
			ALOGD("ctx->dpyAttr[dpy].layer[2].first_set = = false");
			//spritedata[1] = ctx->dpyAttr[dpy].layer[2].ion_fd;
			spritedata[0] = ctx->dpyAttr[dpy].layer[2].ion_fd;;
			if (ioctl(ctx->dpyAttr[dpy].layer[2].fd, RK_FBIOSET_HWC_ADDR, spritedata) == -1)
			{	
		    		ALOGE("%s(%d):  fd[%d] Failed,DataAddr=%x", __FUNCTION__, __LINE__,ctx->dpyAttr[dpy].layer[1].fd,videodata[0]);	
		    		return -errno;
			}
		}
		if(ioctl(ctx->dpyAttr[dpy].layer[2].fd, FBIOPUT_VSCREENINFO, &vinfo) < 0) {
			ALOGE("failed to set framebuffer info\n");
			return -1;
		}
		
	}
	return 0;
}

void hwc_get_screen_size(hwc_context_t *ctx, int dpy)
{
	FILE* fd = NULL;
	char property[PROPERTY_VALUE_MAX];
	fd = fopen("/sys/class/graphics/fb0/screen_info","r");
	if (fd != NULL) {
		memset(property, 0, PROPERTY_VALUE_MAX);
		fgets(property, PROPERTY_VALUE_MAX, fd);
		sscanf(property, "xres:%d", &ctx->dpyAttr[dpy].xres_screen);
		memset(property, 0, PROPERTY_VALUE_MAX);
		fgets(property, PROPERTY_VALUE_MAX, fd);
		sscanf(property, "yres:%d", &ctx->dpyAttr[dpy].yres_screen);
		fclose(fd);
	}
}

int hwc_overlay_ui(hwc_context_t *ctx,\
                   int dpy,\
                   hwc_layer_1_t *src_layer,\
                   struct rk_fb_win_par *win_par)
{
     int area_no = 0;
     int y_offset = 0;
     int x_offset = 0;
     int xsize = 0;
     int ysize = 0;
     int xpos = 0;
     int ypos = 0;
     int xres = 0;
     int yres = 0;
     int xact = 0;
     int yact = 0;
     int v_width = 0;
     int v_height = 0;
     int cur_xsize = 0;
     int cur_ysize = 0;
     int blend_mode = src_layer->blending >> 16;
     hwc_rect_t* src_rect = &src_layer->sourceCrop; //src region
     hwc_rect_t* dst_rect = &src_layer->displayFrame;//dst region
     hwc_rect_t  m_src_rect;
     hwc_rect_t  m_dst_rect;
     int overlay_mode = -1;
     struct private_handle_t* src_handle = (struct private_handle_t *) src_layer->handle;
     char property[PROPERTY_VALUE_MAX];
     if (property_get("sys.fb.cursize", property, NULL) > 0) {
       sscanf(property, "%dx%d", &xres, &yres);
       getCurrScreenInfo(&cur_xsize,&cur_ysize);
     } else {

        getCurrScreenInfo(&cur_xsize,&cur_ysize);
        xres =  ctx->dpyAttr[0].xres;
        yres =  ctx->dpyAttr[0].yres;
     }
      
     if (property_get("video.use.overlay", property, "0") && atoi(property) > 0) {
     	  overlay_mode = atoi(property);
     }

    if (src_layer->compositionType==HWC_FRAMEBUFFER_TARGET) {
       y_offset = src_handle->offset/ctx->dpyAttr[dpy].stride;
       m_src_rect.left = 0;
       m_src_rect.top = 0;
       m_src_rect.right = xres;
       m_src_rect.bottom = yres;
       m_dst_rect.left = 0;
       m_dst_rect.top = 0;
       m_dst_rect.right = xres;
       m_dst_rect.bottom = yres;
       xpos = m_dst_rect.left*cur_xsize/xres;
       ypos = m_dst_rect.top*cur_ysize/yres;
       xsize = (m_dst_rect.right - m_dst_rect.left)*cur_xsize/xres;
       ysize = (m_dst_rect.bottom - m_dst_rect.top)*cur_ysize/yres;
             
       if (overlay_mode == 1) {
         xact = (m_src_rect.right- m_src_rect.left)*cur_xsize/xres;
         yact = (m_src_rect.bottom - m_src_rect.top)*cur_ysize/yres;
       } else {
         xact = (m_src_rect.right- m_src_rect.left);
         yact = (m_src_rect.bottom - m_src_rect.top); 	
       }

     } else {
        y_offset = src_rect->top;
        m_src_rect.left = src_rect->left;
        m_src_rect.top = src_rect->top;
        m_src_rect.right = src_rect->right;
        m_src_rect.bottom = src_rect->bottom;
        m_dst_rect.left = dst_rect->left;
        m_dst_rect.top = dst_rect->top;
        m_dst_rect.right = dst_rect->right;
        m_dst_rect.bottom = dst_rect->bottom;
        xpos = m_dst_rect.left*cur_xsize/xres;
        ypos = m_dst_rect.top*cur_ysize/yres;
        
        if (!strcmp(src_layer->LayerName,"BootAnimation")) {
           xact =  ctx->dpyAttr[0].xres;
           yact =  ctx->dpyAttr[0].yres;
           xsize = cur_xsize;
           ysize = cur_ysize;
        } else {
           xact = m_src_rect.right- m_src_rect.left;
           yact = m_src_rect.bottom - m_src_rect.top;
           xsize = (m_dst_rect.right - m_dst_rect.left)*cur_xsize/xres;
           ysize = (m_dst_rect.bottom - m_dst_rect.top)*cur_ysize/yres;
           
        }
     }
     
     win_par->alpha_mode = AB_SRC_OVER;
     win_par->g_alpha_val = blend_mode;
     
     if (src_handle->format==0x21)
     {
     	// win_par->data_format = HAL_PIXEL_FORMAT_YCrCb_NV12;
         win_par->data_format = HAL_PIXEL_FORMAT_YCrCb_NV12;
     }
     else
     {
     	 if (win_par->z_order==0 && src_handle->format==HAL_PIXEL_FORMAT_RGBA_8888) {
     	 	  // win_par->data_format = HAL_PIXEL_FORMAT_RGBX_8888;
             win_par->data_format = HAL_PIXEL_FORMAT_RGBX_8888;
     	 }
     	 else
     	 {
           //win_par->data_format = src_handle->format;
            win_par->data_format = src_handle->format;
         }
     }
     /*if (!strcmp(src_layer->LayerName,"Sprite") && false) {
     	  win_par->area_par[area_no].ion_fd = ctx->dpyAttr[0].layer[2].ion_fd;
     	  xsize = 32;//*cur_xsize/xres;
     	  ysize = 32;//*cur_ysize/yres;
     	  xact = 32;
     	  yact = 32;
     	  v_width = 32;
     	  v_height = 32;
	    char property[PROPERTY_VALUE_MAX];
	    if (property_get("video.use.overlay", property, "0") && atoi(property)==2) {
     	          xpos = dst_rect->left*cur_xsize/xres;
     	          ypos = dst_rect->top*cur_ysize/yres;
	    }
	    else{
		   xpos = dst_rect->left;
		   ypos = dst_rect->top;
	    }
            m_src_rect.left = 0;
     }
     else
     {*/
     	  win_par->area_par[area_no].ion_fd = src_handle->share_fd;
     	  v_width = src_handle->stride;
     	  v_height = src_handle->height; 
     //}
   
     win_par->area_par[area_no].phy_addr = 0;
     win_par->area_par[area_no].acq_fence_fd = -1;
     win_par->area_par[area_no].x_offset = m_src_rect.left;
     if (win_par->data_format == HAL_PIXEL_FORMAT_YCrCb_NV12 || win_par->data_format==0x22)
        win_par->area_par[area_no].x_offset = m_src_rect.left - m_src_rect.left%2;
     else
        win_par->area_par[area_no].x_offset = m_src_rect.left;
     win_par->area_par[area_no].y_offset = y_offset;
     win_par->area_par[area_no].xpos =  xpos;
     win_par->area_par[area_no].ypos = ypos;
     win_par->area_par[area_no].xsize = xsize;
     win_par->area_par[area_no].ysize = ysize;
     win_par->area_par[area_no].xact = xact;
     win_par->area_par[area_no].yact = yact;
     win_par->area_par[area_no].xvir = v_width;//src_handle->stride;
     win_par->area_par[area_no].yvir = v_height;//src_handle->height;
     ALOGV("=====(%d,%d,%d,%d)(%d,%d,%d,%d)",\
            src_rect->left,src_rect->top,src_rect->right,src_rect->bottom,\
            dst_rect->left,dst_rect->top,dst_rect->right,dst_rect->bottom);
     dump_win_par(win_par,ctx->dpyAttr[dpy].layer[0].fd,0);
     return 0;
}

int hwc_overlay_video(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src)
{
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;
	struct tVPU_FRAME *pFrame  = NULL;	
	hwc_rect_t * DstRect = &(Src->displayFrame);
	int enable, nonstd, grayscale;
	
	ALOGD_IF(HWC_DEBUG, "%s format %x width %d height %d address 0x%x", __FUNCTION__, srchnd->format, srchnd->width, srchnd->height, srchnd->base);

	if(srchnd->format == HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO || srchnd->format == 0x22 || srchnd->format==0x20) {
		pFrame = (tVPU_FRAME *)srchnd->base;
		ALOGD_IF(HWC_DEBUG, "%s video Frame addr=%x,FrameWidth=%d,FrameHeight=%d DisplayWidth=%d, DisplayHeight=%d, FrameRate=%d",
		 __FUNCTION__, pFrame->FrameBusAddr[0], pFrame->FrameWidth, pFrame->FrameHeight, pFrame->DisplayWidth, pFrame->DisplayHeight, pFrame->DecodeFrmNum);
	}
	else
		return 0;
	
	ALOGD_IF(HWC_DEBUG, "%s DstRect %d %d %d %d", __FUNCTION__, DstRect->left, DstRect->top, DstRect->right, DstRect->bottom);
	
	if(ctx->dpyAttr[dpy].layer[1].active == false) {
		if(hwc_enable_layer(ctx, dpy, 1, 1))
			return -1;
		videodata[0] = 0;
		videodata[1] = 0;
		hwc_save_hdmi_mode(ctx);
	}

	if(pFrame->DecodeFrmNum == 23)
		pFrame->DecodeFrmNum = 24;
	else if(pFrame->DecodeFrmNum == 29)
		pFrame->DecodeFrmNum = 30;
	else if(pFrame->DecodeFrmNum == 59)
		pFrame->DecodeFrmNum = 60;
	hwc_set_hdmi_mode(ctx, pFrame->DecodeFrmNum);

	struct fb_var_screeninfo info;
        char property[PROPERTY_VALUE_MAX];
        int cur_xsize = 0;
        int cur_ysize = 0;
        int xres = 0;
        int yres = 0;
        if (property_get("sys.fb.cursize", property, NULL) > 0) {
          sscanf(property, "%dx%d", &xres, &yres);
          getCurrScreenInfo(&cur_xsize,&cur_ysize);
        } else {
          getCurrScreenInfo(&cur_xsize,&cur_ysize);
          xres =  ctx->dpyAttr[0].xres;
          yres =  ctx->dpyAttr[0].yres; 	
        }
        memset(property,0,sizeof(property));
        if (property_get("persist.sys.display.policy", property, NULL) > 0) {
          if (!strcmp(property, "manual")) {
             cur_xsize = xres;
             cur_ysize = yres;
          }
        }        	
	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOGET_VSCREENINFO, &info) == -1)
	{
		ALOGE("%s(%d):  fd[%d] Failed", __FUNCTION__, __LINE__, ctx->dpyAttr[dpy].layer[1].fd);
        	return -1;
	}
        if (ctx->overlaymode==2)
        {
	      nonstd = ((DstRect->left*cur_xsize/xres)<<8) + ((DstRect->top*cur_ysize/yres)<<20);
	      grayscale = (((DstRect->right - DstRect->left)*cur_xsize/xres) << 8) + (((DstRect->bottom - DstRect->top)*cur_ysize/yres) << 20);
	 } else {
           nonstd = ((DstRect->left/*cur_xsize/xres*/)<<8) + ((DstRect->top/*cur_ysize/yres*/)<<20);
           grayscale = (((DstRect->right - DstRect->left)/*cur_xsize/xres*/) << 8) + (((DstRect->bottom - DstRect->top)/*cur_ysize/yres*/) << 20); 
        }
        
	info.activate = FB_ACTIVATE_NOW;
        info.activate |= FB_ACTIVATE_FORCE;    		
	info.nonstd &= 0x00;
	if(srchnd->format == 0x22 || pFrame->OutputWidth==0x22)
		info.nonstd |= 0x22;
	else
		info.nonstd |= HAL_PIXEL_FORMAT_YCrCb_NV12;		
	info.nonstd |= nonstd;
	info.grayscale &= 0xff;
	info.grayscale |= grayscale;
       if (srchnd->format != 0x20) {	
	info.xoffset = 0;
	info.yoffset = 0;
	info.xres = pFrame->DisplayWidth;
	info.yres = pFrame->DisplayHeight;
	info.xres_virtual = pFrame->FrameWidth;
	info.yres_virtual = pFrame->FrameHeight;
//	info.rotate = ;
	/* Check yuv format. */
	if(videodata[0] != pFrame->FrameBusAddr[0]) {
		videodata[0] = pFrame->FrameBusAddr[0];
		videodata[1] = pFrame->FrameBusAddr[1];
//		if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, RK_FBIOSET_YUV_ADDR, videodata) == -1)
//		{	
//	    		ALOGE("%s(%d):  fd[%d] Failed,DataAddr=%x", __FUNCTION__, __LINE__,ctx->dpyAttr[dpy].layer[1].fd,videodata[0]);	
//	    		return -errno;
//		}
	}
 } else {
    	info.xres = Src->sourceCrop.right - Src->sourceCrop.left; //DstIRect->right - DstRect->left;//pFrame->DisplayWidth;
        info.yres = Src->sourceCrop.bottom - Src->sourceCrop.top;//DstRect->bottom - DstRect->top;//pFrame->DisplayHeight;
        info.xres_virtual = Src->sourceCrop.right - Src->sourceCrop.left;//pFrame->FrameWidth;
        info.yres_virtual = Src->sourceCrop.bottom - Src->sourceCrop.top;//pFrame->FrameHeight;
 }
	//if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOPUT_VSCREENINFO, &info) == -1)
	 //   return -errno;
//	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, RK_FBIOSET_CONFIG_DONE, NULL) == -1)
//		return -errno;
	return 0;
}
int hwc_overlay(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src)
{	
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;
	struct tVPU_FRAME *pFrame  = NULL;	
	hwc_rect_t * DstRect = &(Src->displayFrame);
	int enable, nonstd, grayscale;
	int left, top, width, height;

	ALOGD_IF(HWC_DEBUG, "%s format %x width %d height %d address 0x%x", __FUNCTION__, srchnd->format, srchnd->width, srchnd->height, srchnd->base);

	if(srchnd->format == HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO || srchnd->format == 0x22) {
		pFrame = (tVPU_FRAME *)srchnd->base;
		ALOGD_IF(HWC_DEBUG, "%s video Frame addr=%x,FrameWidth=%d,FrameHeight=%d DisplayWidth=%d, DisplayHeight=%d, FrameRate=%d",
		 __FUNCTION__, pFrame->FrameBusAddr[0], pFrame->FrameWidth, pFrame->FrameHeight, pFrame->DisplayWidth, pFrame->DisplayHeight, pFrame->DecodeFrmNum);
	}
	else
		return 0;
	
	ALOGD_IF(HWC_DEBUG, "%s DstRect %d %d %d %d", __FUNCTION__, DstRect->left, DstRect->top, DstRect->right, DstRect->bottom);
	
	if(ctx->dpyAttr[dpy].layer[1].active == false) {
		if(hwc_enable_layer(ctx, dpy, 1, 1))
			return -1;
		videodata[0] = 0;
		videodata[1] = 0;
		hwc_save_hdmi_mode(ctx);
	}

	if(pFrame->DecodeFrmNum == 23)
		pFrame->DecodeFrmNum = 24;
	else if(pFrame->DecodeFrmNum == 29)
		pFrame->DecodeFrmNum = 30;
	else if(pFrame->DecodeFrmNum == 59)
		pFrame->DecodeFrmNum = 60;
	hwc_set_hdmi_mode(ctx, pFrame->DecodeFrmNum);

	struct fb_var_screeninfo info;
	
	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOGET_VSCREENINFO, &info) == -1)
	{
		ALOGE("%s(%d):  fd[%d] Failed", __FUNCTION__, __LINE__, ctx->dpyAttr[dpy].layer[1].fd);
        	return -1;
	}
	left =  DstRect->left;
	top = DstRect->top;
	width = DstRect->right - DstRect->left;
	height = DstRect->bottom - DstRect->top;
	if (ctx->overlaymode != 1) {
		left = left * ctx->dpyAttr[dpy].xres_screen / ctx->dpyAttr[dpy].xres;
		top = top * ctx->dpyAttr[dpy].yres_screen / ctx->dpyAttr[dpy].yres;
		width = width * ctx->dpyAttr[dpy].xres_screen / ctx->dpyAttr[dpy].xres;
		height = height * ctx->dpyAttr[dpy].yres_screen / ctx->dpyAttr[dpy].yres;
	} else if(ctx->dpyAttr[dpy].xres_screen >= pFrame->DisplayWidth &&
		  ctx->dpyAttr[dpy].yres_screen >= pFrame->DisplayHeight) {
		char property[PROPERTY_VALUE_MAX];
		memset(property, 0, PROPERTY_VALUE_MAX);
		property_get("persist.sys.video.fullscreen", property, NULL);
		if(atoi(property) == 1){
			left = (ctx->dpyAttr[dpy].xres_screen - pFrame->DisplayWidth)/2;
			top = ((ctx->dpyAttr[dpy].yres_screen - pFrame->DisplayHeight)/2);
			width = pFrame->DisplayWidth;
			height = pFrame->DisplayHeight;
		}
	}
	
	nonstd = (left<<8) + (top<<20);
	grayscale = (width << 8) + (height << 20);
	
	info.activate = FB_ACTIVATE_NOW;
	if(videodata[0] && (nonstd != (info.nonstd & 0xffffff00) || grayscale != (info.grayscale & 0xffffff00)) ) {
		info.activate |= FB_ACTIVATE_FORCE;
		ALOGD("active on now");
	}
	else
		info.activate |= FB_ACTIVATE_VBL;
		
	info.nonstd &= 0x00;
	if(srchnd->format == 0x22 || pFrame->OutputWidth==0x22)
		info.nonstd |= 0x22;
	else
		info.nonstd |= HAL_PIXEL_FORMAT_YCrCb_NV12;		
	info.nonstd |= nonstd;
	info.grayscale &= 0xff;
	info.grayscale |= grayscale;
	
	info.xoffset = 0;
	info.yoffset = 0;
	info.xres = pFrame->DisplayWidth;
	info.yres = pFrame->DisplayHeight;
	info.xres_virtual = pFrame->FrameWidth;
	info.yres_virtual = pFrame->FrameHeight;
//	info.rotate = ;
	/* Check yuv format. */
	if(videodata[0] != pFrame->FrameBusAddr[0]) {
		videodata[0] = pFrame->FrameBusAddr[0];
		videodata[1] = pFrame->FrameBusAddr[1];
//		if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, RK_FBIOSET_YUV_ADDR, videodata) == -1)
//		{	
//	    		ALOGE("%s(%d):  fd[%d] Failed,DataAddr=%x", __FUNCTION__, __LINE__,ctx->dpyAttr[dpy].layer[1].fd,videodata[0]);	
//	    		return -errno;
//		}
	}
	
	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOPUT_VSCREENINFO, &info) == -1)
	    return -errno;
	    
//	if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, RK_FBIOSET_CONFIG_DONE, NULL) == -1)
//		return -errno;
	return 0;
}

int hwc_postfb(hwc_context_t *ctx, int dpy, hwc_layer_1_t *Src)
{
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;	
	hwc_rect_t * DstRect = &(Src->displayFrame);
	int left, top, width, height;
	int nonstd, grayscale;	

	if(dpy == 0 && srchnd) {
		ALOGD_IF(HWC_DEBUG, "%s format %x width %d height %d address 0x%x offset 0x%x", __FUNCTION__, srchnd->format, srchnd->width, srchnd->height, srchnd->base, srchnd->offset);
		
		struct fb_var_screeninfo info;
		if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, FBIOGET_VSCREENINFO, &info) == -1)
	    		return -errno;	
	
	    left =  DstRect->left;
		top = DstRect->top;
		width = DstRect->right - DstRect->left;
		height = DstRect->bottom - DstRect->top;
		if (ctx->overlaymode != 1) {
			left = left * ctx->dpyAttr[dpy].xres_screen / ctx->dpyAttr[dpy].xres;
			top = top * ctx->dpyAttr[dpy].yres_screen / ctx->dpyAttr[dpy].yres;
			width = width * ctx->dpyAttr[dpy].xres_screen / ctx->dpyAttr[dpy].xres;
			height = height * ctx->dpyAttr[dpy].yres_screen / ctx->dpyAttr[dpy].yres;
		}
//		ALOGE("screen info is %d %d %d %d", left, top, width, height);
		nonstd = (left<<8) + (top<<20);
		grayscale = (width << 8) + (height << 20);
#ifdef TARGET_RK32
        info.activate = FB_ACTIVATE_NOW | FB_ACTIVATE_FORCE;
		info.nonstd = HAL_PIXEL_FORMAT_RGBA_8888;
#else
		info.nonstd &= 0xff;
#endif
		info.nonstd |= nonstd;
	    info.grayscale &= 0xff;
		info.grayscale |= grayscale;
	    	info.yoffset = srchnd->offset/ctx->dpyAttr[dpy].stride;
		info.activate = FB_ACTIVATE_NOW | FB_ACTIVATE_FORCE;
		if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, FBIOPUT_VSCREENINFO, &info) == -1)
			return -errno;
//		if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, RK_FBIOSET_CONFIG_DONE, NULL) == -1)
//			return -errno;
	}

	return 0;
}

int hwc_yuv2rgb(hwc_context_t *ctx, hwc_layer_1_t *Src)
{
	struct private_handle_t* srchnd = (struct private_handle_t *) Src->handle;
	
	if(ctx->mCopyBit == NULL) {
		ALOGE("%s device not ready.", __FUNCTION__);
		return -1;
	}
	
	if(srchnd == NULL || srchnd->format != HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO) {
		ALOGE("%s no need to do color convert.", __FUNCTION__);
		return 0;
	}
	
	struct tVPU_FRAME *pFrame  = (tVPU_FRAME *)srchnd->base;
	ALOGD_IF(HWC_DEBUG, "%s video Frame addr=%x,FrameWidth=%u,FrameHeight=%u DisplayWidth=%u, DisplayHeight=%u",
	 __FUNCTION__, pFrame->FrameBusAddr[0], pFrame->FrameWidth, pFrame->FrameHeight, pFrame->DisplayWidth, pFrame->DisplayHeight);
	
	if(pFrame->FrameWidth > 3840 || pFrame->FrameHeight > 2160 || pFrame->FrameBusAddr[0] == 0xFFFFFFFF) {
		ALOGE("%s error parameter, cannot convert.", __FUNCTION__);
		return -1;
	}
	
//	memset((void*)srchnd->base, 0xFF, srchnd->width * srchnd->height * 4);
//	return 0;
	struct _rga_img_info_t src, dst;
	memset(&src, 0, sizeof(struct _rga_img_info_t));
	memset(&dst, 0, sizeof(struct _rga_img_info_t));

#ifdef TARGET_RK32
	src.yrgb_addr =	(int)pFrame->FrameBusAddr[0]; 
	dst.format = RK_FORMAT_YCbCr_420_SP;
#else
	src.yrgb_addr =	(int)pFrame->FrameBusAddr[0]+ 0x60000000;
	dst.format = RK_FORMAT_RGBA_8888;
#endif
	src.uv_addr	= src.yrgb_addr + ((pFrame->FrameWidth + 15)&(~15)) * ((pFrame->FrameHeight+ 15)&(~15));
	src.v_addr   = src.uv_addr;
	src.vir_w = (pFrame->FrameWidth + 15)&(~15);
	src.vir_h = (pFrame->FrameHeight + 15)&(~15);
	src.format = RK_FORMAT_YCbCr_420_SP;
	src.act_w = pFrame->DisplayWidth;
	src.act_h = pFrame->DisplayHeight;
	src.x_offset = 0;
	src.y_offset = 0;
	

	dst.yrgb_addr = (uint32_t)srchnd->base;
	dst.uv_addr  = 0;
	dst.v_addr   = 0;	
   	dst.vir_w = ((srchnd->width*2 + (8-1)) & ~(8-1))/2;
	dst.vir_h = srchnd->height;
	dst.act_w = pFrame->DisplayWidth;
	dst.act_h = pFrame->DisplayHeight;
	dst.x_offset = 0;
	dst.y_offset = 0;
#ifndef TARGET_RK32
	return ctx->mCopyBit->draw(&src, &dst, RK_MMU_SRC_ENABLE | RK_MMU_DST_ENABLE | RK_BT_601_MPEG);
#else
	return ctx->mCopyBit->draw(&src, &dst, RK_MMU_DST_ENABLE | RK_BT_601_MPEG);
#endif
	
}

int hwc_enable_layer(hwc_context_t *ctx, int dpy, int layer, int active)
{
//	if (ioctl(ctx->dpyAttr[dpy].layer[layer].fd, RK_FBIOSET_ENABLE, &active) == -1)
//		return -errno;
	
//	if (ioctl(ctx->dpyAttr[dpy].layer[layer].fd, RK_FBIOSET_CONFIG_DONE, NULL) == -1)
//		return -errno;
	
	ctx->dpyAttr[dpy].layer[layer].active = active;
	
	return 0;
}
