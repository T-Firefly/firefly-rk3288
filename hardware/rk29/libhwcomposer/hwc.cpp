/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <poll.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <cutils/log.h>
#include <cutils/atomic.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <cutils/properties.h>
#include <EGL/egl.h>

#include "hwc.h"
#include "../libgralloc_ump/gralloc_priv.h"
/*****************************************************************************/
#define  ENABLE_LCDC_COUNT  2

static int g_composer_mode = 0;
static int winid_order1[] = {0,1};
static int winid_order2[] = {1,0};
static int hwc_device_open(const struct hw_module_t* module, const char* name,
        struct hw_device_t** device);

static struct hw_module_methods_t hwc_module_methods = {
    open: hwc_device_open
};

hwc_module_t HAL_MODULE_INFO_SYM = {
    common: {
        tag: HARDWARE_MODULE_TAG,
        version_major: 1,
        version_minor: 0,
        id: HWC_HARDWARE_MODULE_ID,
        name: "Hardware Composer Module",
        author: "RockChip Box Team",
        methods: &hwc_module_methods,
      	dso: 0,
        reserved: {0},
    }
};

/*****************************************************************************/

static void dump_layer(hwc_layer_1_t const* l) {
    ALOGD("\ttype=%d, flags=%08x, handle=%p, tr=%02x, blend=%04x, {%d,%d,%d,%d}, {%d,%d,%d,%d}",
            l->compositionType, l->flags, l->handle, l->transform, l->blending,
            l->sourceCrop.left,
            l->sourceCrop.top,
            l->sourceCrop.right,
            l->sourceCrop.bottom,
            l->displayFrame.left,
            l->displayFrame.top,
            l->displayFrame.right,
            l->displayFrame.bottom);
}

static void dump_list(hwc_display_contents_1_t *list) {
   ALOGD_IF(OVERLAY_DEBUG,"-------------------------------------------");
   for (uint32_t i = 0; i < list->numHwLayers; i++)
   {
        hwc_layer_1_t* layer = &(list->hwLayers[i]);
        ALOGD_IF(OVERLAY_DEBUG,"----layername=%s,index=%d,Acqfd=%d,composertype=%d,g_composer_mode=%d",\
              layer->LayerName,i,layer->acquireFenceFd,layer->compositionType,g_composer_mode);
   }
   ALOGD_IF(OVERLAY_DEBUG,"-------------------------------------------");
}
//chips have a sprint-layer(32x32 or 64x64)
static int enable_hardware_sprite(hwc_display_contents_1_t *list)
{
    return -1;  //3288
    for (uint32_t i = 0; i < list->numHwLayers; i++) {
    	 hwc_layer_1_t* layer = &(list->hwLayers[i]);
    	 if (!strcmp(layer->LayerName, "Sprite")) {
    	 	 layer->compositionType = HWC_OVERLAY;
    	 	 return i;
    	 }
    }
    return -1;
}

static int is_scaled_layer(hwc_layer_1_t *layer) {
	
	  hwc_rect_t *dstRect = &(layer->displayFrame);
      hwc_rect_t *srcRect = &(layer->sourceCrop);
	  int src_w = srcRect->right - srcRect->left;
	  int src_h = srcRect->bottom - srcRect->top;
      int dst_w = dstRect->right - dstRect->left;
	  int dst_h = dstRect->bottom - dstRect->top;
	  float rate_w = (float)dst_w/src_w;
	  float rate_h = (float)dst_h/src_h;
	  if (rate_w>=8 || rate_h>=8) {
	  	 ALOGD_IF(OVERLAY_DEBUG,"Scale layer:name=%s",layer->LayerName);
	  	 return true;
	  }
	  return false;
}

static int hwc_region_priority(hwc_display_contents_1_t *list) {
    #ifdef RK312X
     return -1;
    #endif
   int max_region_index = 0;
   int max_region = 0;
   int layer_count = list->numHwLayers-1;
   for (int i=0;i<layer_count;i++) {
       hwc_layer_1_t* layer = &(list->hwLayers[i]);
       struct private_handle_t *handle = (struct private_handle_t *) layer->handle;
       if (handle==NULL ||strstr(layer->LayerName,"Starting")){
          ALOGV("handle is null,i=%d,name=%s",i,layer->LayerName);
        //  continue;
          return -1;
       }
       if (handle->format == HAL_PIXEL_FORMAT_YV12) {
         return -1;
       }
       /*if (strcmp(layer->LayerName,"Sprite") != 0) {
           continue;
       } else {
       	  return i;
       }*/

              
       if (is_scaled_layer(layer) && handle->format==0x20)
       {
         return -1;	
       }
       
       ALOGV("i=%d,name=%s,ionfd=%d,handle=%x",i,layer->LayerName,handle->share_fd,handle);
       if (layer->blending!=HWC_BLENDING_NONE && i<(layer_count-1)) {
          ALOGD_IF(OVERLAY_DEBUG,"alpha is name=%s,planeAlpha=%x",layer->LayerName,layer->planeAlpha);
          continue;
       }

       
       hwc_rect_t* src_rect = &layer->sourceCrop; //src region
       int width = src_rect->right - src_rect->left;
       int height = src_rect->bottom - src_rect->top;
       int R = width*height;
       if (max_region < R)
       {
         max_region = R;
         max_region_index = i;
       }
       
       if (handle->format == 0x20) {
          max_region_index = i;
          break;
       }
   }
   ALOGV("max_region=%d,layername=%s",max_region,list->hwLayers[max_region_index].LayerName);
   max_region_index = 0;
   return max_region_index;
}

static int hwc_check_win_order(struct rk_fb_win_cfg_data *fb_info, hwc_display_contents_1_t *list) {

   if (fb_info==NULL)
       return -1;
   int enable_hard_sprint = enable_hardware_sprite(list);
   int sprint_winid = -1;
   int winid = 0;
   int overlay_id = 0;
   int z_order = 0;
   for (int i=0; i<(int)list->numHwLayers; i++)
   {
     hwc_layer_1_t* layer = &(list->hwLayers[i]);
     switch (g_composer_mode) {
      case ALL_TO_GPU:
      	if (layer->compositionType==HWC_OVERLAY) {//hard sprint
      		if (enable_hard_sprint==i) {
      		    fb_info->win_par[winid].win_id = 2;
                    fb_info->win_par[winid].z_order = 0;	
                    sprint_winid = winid;
                    winid++;
      		}
      	} else if (layer->compositionType==HWC_FRAMEBUFFER_TARGET) {
      	     fb_info->win_par[winid].win_id = winid_order1[overlay_id];
             fb_info->win_par[winid].z_order = z_order;
             z_order++;
             if (enable_hard_sprint>=0 && sprint_winid>=0) {
            	 fb_info->win_par[sprint_winid].z_order = z_order;	
             }
      	} else {
      		
      	}
        break;
      case ALL_TO_LCDC:
      //case ONE_TO_LCDC:
      	if (layer->compositionType==HWC_OVERLAY) {
      		if (enable_hard_sprint==i) {
      		    fb_info->win_par[winid].win_id = 2;
                    fb_info->win_par[winid].z_order = 0;	
                    sprint_winid = winid;
                    winid++;
      		} else {
      	            fb_info->win_par[winid].win_id = winid_order1[overlay_id];
                    fb_info->win_par[winid].z_order = z_order;	
                    winid++;
                    z_order++;
                    overlay_id++;
                    if (enable_hard_sprint>=0 && sprint_winid>=0) {
            	       fb_info->win_par[sprint_winid].z_order = z_order;	
                    }
                    struct private_handle_t *handle = (struct private_handle_t *) layer->handle;
	            if (i > 0 && handle->format==0x20) {
                      fb_info->win_par[1].win_id = 0;
		      fb_info->win_par[1].z_order = 1;
		      fb_info->win_par[0].win_id = 1;
		      fb_info->win_par[0].z_order = 0;
                    }
                }
      	} 
      	break;
      case ONE_TO_LCDC:
        if (layer->compositionType==HWC_OVERLAY) {
                if (enable_hard_sprint==i) {
                    fb_info->win_par[winid].win_id = 2;
                    fb_info->win_par[winid].z_order = 0;
                    sprint_winid = winid;
                    winid++;
                } else {
                    fb_info->win_par[winid].win_id = winid_order1[overlay_id];
                    fb_info->win_par[winid].z_order = z_order;
                    winid++;
                    z_order++;
                    overlay_id++;
                    if (enable_hard_sprint>=0 && sprint_winid>=0) {
                       fb_info->win_par[sprint_winid].z_order = z_order;
                    }
                }
        }
        break;

      case MIX_TO_GPU:
      case VIDEO_TO_LCDC:
  	    if (layer->compositionType==HWC_OVERLAY) {
      		if (enable_hard_sprint==i) {
      		    fb_info->win_par[winid].win_id = 2;
                    fb_info->win_par[winid].z_order = 0;	
                    sprint_winid = winid;
                    winid++;
      		}else {
      	           fb_info->win_par[winid].win_id = winid_order1[overlay_id];
                   fb_info->win_par[winid].z_order = z_order;	
                   winid++;
                   z_order++;
                   overlay_id++;
      		}
      	 } else if (layer->compositionType==HWC_FRAMEBUFFER_TARGET) {
            fb_info->win_par[winid].win_id = winid_order1[overlay_id];
            fb_info->win_par[winid].z_order = z_order;
            winid++;
            z_order++;
            overlay_id++;
      	 } else {
      		
      	 }
      	if (enable_hard_sprint>=0 && sprint_winid>=0 && i==(int)(list->numHwLayers-1)) {
            	fb_info->win_par[sprint_winid].z_order = z_order;	
        }
        break;
        
      default:
        break;
   }
  }
   ALOGD_IF(OVERLAY_DEBUG,"%s,hwc compose type=%d",__FUNCTION__,g_composer_mode);

   return 0;
}

static int has_tranformed_layer(hwc_display_contents_1_t *list) {
     int layer_count = (int)(list->numHwLayers-1);
     for (int i=0;i<layer_count;i++) {
            hwc_layer_1_t* layer = &(list->hwLayers[i]);
            if (layer->transform != 0) {
               return true;
            }
     }

     return false;
}
static int hwc_check_policy(hwc_display_contents_1_t *list) {
  	
    int j = hwc_region_priority(list);
    int layer_count = (int)(list->numHwLayers-1);
    int enable_hard_sprint = enable_hardware_sprite(list);
    int r = has_tranformed_layer(list);
    if (r > 0) {
         g_composer_mode = ALL_TO_GPU;
         
		 return 0;
    }
    if (enable_hard_sprint >= 0) {
    	 layer_count = layer_count-1;
    }
    if ((layer_count <= ENABLE_LCDC_COUNT) && j>=0) {
    	  int count = 0;
        for (int i=0;i<layer_count;i++) {
            hwc_layer_1_t* layer = &(list->hwLayers[i]);
            struct private_handle_t *handle = (struct private_handle_t *) layer->handle;
            if (handle) {
                layer->compositionType = HWC_OVERLAY;
                layer->hints |= HWC_HINT_CLEAR_FB;  
                g_composer_mode = ALL_TO_LCDC;
                count++;
            } 
        }
        
        if (count==1) {
           g_composer_mode = ONE_TO_LCDC;
        }
 
    } else {//mix mode
       if (j >= 0) {
         hwc_layer_1_t* layer = &list->hwLayers[j];
         layer->compositionType = HWC_OVERLAY;
         layer->hints |= HWC_HINT_CLEAR_FB;  
         g_composer_mode = MIX_TO_GPU;
         ALOGD_IF(OVERLAY_DEBUG, "%s,layer %s to overlay.", __FUNCTION__, layer->LayerName);
       } else {
       	  g_composer_mode = ALL_TO_GPU;
       } 
    }
    return 1;
}

static int hwc_enable_display(hwc_context_t* ctx,hwc_layer_1_t* layer)
{
	
	const int dpy = HWC_DISPLAY_PRIMARY;			  
	char property[PROPERTY_VALUE_MAX];	
	memset(property, 0, PROPERTY_VALUE_MAX);
	property_get("sys.hwc.bootanim.noblank",property,"0");
	if(ctx->dpyAttr[0].enabledisplay == false && (!strcmp(property,"true"))){		
		struct private_handle_t *handle = (struct private_handle_t *) layer->handle;
		int fbsize = (ctx->gralloc_module->info.xres) * (ctx->gralloc_module->info.yres) * 4;
		char *black = (char *)malloc(fbsize);
		for(int i=0;i++;i<fbsize)
			black[i]=0;
		if(memcmp((const void *)handle->base, black, fbsize) != 0){
			ctx->dpyAttr[0].enabledisplay = true;
			//ctx->gralloc_module->info.yoffset = handle->offset/ctx->dpyAttr[dpy].stride;
			if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, FBIOPUT_VSCREENINFO, &ctx->gralloc_module->info) == -1) {
				ALOGW("FBIOPUT_VSCREENINFO failed, page flipping not supported");
			}
			hwc_fb_swap();
			//hwc_set_fd(ctx);
		}
		if(black != NULL)				
		{
			free(black);
			black = NULL;
		}
	}else{
		ctx->dpyAttr[0].enabledisplay = true;
	}
	return 0;
}
static int hwc_prepare_primary(hwc_composer_device_1 *dev,
        hwc_display_contents_1_t *list) {
	hwc_context_t* ctx = (hwc_context_t*)(dev);
    g_composer_mode = ALL_TO_GPU;
     int video_no_overlay = 0;
    for (uint32_t i = 0; i < list->numHwLayers; i++)
    {
        hwc_layer_1_t* layer = &(list->hwLayers[i]);
        struct private_handle_t *handle = (struct private_handle_t *) layer->handle;
        if(handle)
        	ALOGD_IF(HWC_DEBUG, "%s layer %d format %x", __FUNCTION__, i, handle->format);
		//ALOGD_IF(1, "%s layer->name = %s ", __FUNCTION__,layer->LayerName);
        //dump_layer(layer);      
        if(handle && (handle->format == HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO/* || handle->format == 0x22 || handle->format==0x20*/) ) {
        	char property[PROPERTY_VALUE_MAX];
        	memset(property, 0, PROPERTY_VALUE_MAX);
        	if(property_get("video.use.overlay", property, NULL) <= 0 || atoi(property) == 0)
                {  
        		//hwc_yuv2rgb(ctx, layer);
                        video_no_overlay = 1;
                }
        	else {
        		//layer->compositionType = HWC_OVERLAY;
        		//layer->hints |= HWC_HINT_CLEAR_FB;
              //          g_composer_mode = VIDEO_TO_LCDC;
        	}
        }/*else if(strcmp(layer->LayerName,"Sprite") == 0){
    		//ALOGD("layer->LayerName = %s *******************",layer->LayerName);
    		char property[PROPERTY_VALUE_MAX];
        memset(property, 0, PROPERTY_VALUE_MAX);
			  property_get("persist.sys.use.hwcursor", property, NULL);
			  //if(property != NULL && atoi(property) > 0)  
			  {
    			layer->compositionType = HWC_OVERLAY;
    			//layer->hints |= HWC_HINT_CLEAR_FB;
    		}
    	 }*/
         if (handle && handle->format == 0x22) {
            layer->compositionType = HWC_OVERLAY;
            layer->hints |= HWC_HINT_CLEAR_FB;
            g_composer_mode = VIDEO_TO_LCDC;
            ALOGD("===============================VIDEO_TO_LCDC");
         }	
    }

    if (g_composer_mode!=VIDEO_TO_LCDC) //video to win0,others to win1 by gpu,so don't need it.
    {
      hwc_check_policy(list);
    }
    if (g_composer_mode==ALL_TO_GPU) {
       
      for (uint32_t i = 0; i < list->numHwLayers-1; i++)
       { 
          hwc_layer_1_t* layer = &(list->hwLayers[i]);
          layer->compositionType = 0;
       }  
    }

    
  //  hwc_dump_layer(list);
   // enable_hardware_sprite(list);

    return 0;
}
/*
static int video_exit_flag = 0;
void reset_video_info(hwc_context_t *ctx,int dpy) {
    struct fb_var_screeninfo info;

    if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOGET_VSCREENINFO, &info) == -1)
    {
        ALOGE("%s(%d):  fd[%d] Failed", __FUNCTION__, __LINE__, ctx->dpyAttr[dpy].layer[1].fd);
       return;
    }
    info.activate = 0;

   if (ioctl(ctx->dpyAttr[dpy].layer[1].fd, FBIOPUT_VSCREENINFO, &info) == -1)
   {
     ALOGE("%s(%d):  fd[%d] Failed", __FUNCTION__, __LINE__, ctx->dpyAttr[dpy].layer[1].fd);  
     return ;
   }
}*/
static int hwc_prepare(hwc_composer_device_1_t *dev,
        size_t numDisplays, hwc_display_contents_1_t** displays) {
        	
     int ret;
     for (int32_t i = numDisplays - 1; i >= 0; i--) {
        hwc_display_contents_1_t *list = displays[i];
        switch(i) {
            case HWC_DISPLAY_PRIMARY:
                ret = hwc_prepare_primary(dev, list);
                break;
//            case HWC_DISPLAY_EXTERNAL:
//                ret = hwc_prepare_external(dev, list, i);
//                break;
//            case HWC_DISPLAY_VIRTUAL:
//                ret = hwc_prepare_virtual(dev, list, i);
//                break;
            default:
                ret = -EINVAL;
        }
    }
     return 0;

}
static int hwc_set_virtual(hwc_context_t *ctx, hwc_display_contents_1_t* list) {
     for (uint32_t i = 0; i < list->numHwLayers; i++)
     {
        if (list->hwLayers[i].acquireFenceFd >0 )
                    sync_wait(list->hwLayers[i].acquireFenceFd, -1);

           close(list->hwLayers[i].acquireFenceFd);
            list->hwLayers[i].acquireFenceFd = -1;
     }

     if (list->outbufAcquireFenceFd>0)
     {
        ALOGV(">>>close outbufAcquireFenceFd:%d",list->outbufAcquireFenceFd);
        close(list->outbufAcquireFenceFd);
        list->outbufAcquireFenceFd = -1;
     }
    
     return 0;

}
    

static int hwc_set_primary(hwc_context_t *ctx, hwc_display_contents_1_t* list) {
	const int dpy = HWC_DISPLAY_PRIMARY;
	int overlay_flag = 0;
        int video_flag = 0;
        int ui_overlay_flag = 0;
        bool NeedSwap = false;
        int ret = 0;
        struct rk_fb_win_cfg_data fb_info;
        memset(&fb_info, 0, sizeof(rk_fb_win_cfg_data));
        int winid = 0;
        int sprite_flag = 0;
        hwc_check_win_order(&fb_info,list);
	    hwc_get_screen_size(ctx, dpy);
	for (uint32_t i = 0; i < list->numHwLayers; i++) {
		if (list->hwLayers[i].acquireFenceFd >0 )
				sync_wait(list->hwLayers[i].acquireFenceFd, -1);
		switch (list->hwLayers[i].compositionType)
		{
		    case HWC_OVERLAY:
		    {
		            /* TODO: HANDLE OVERLAY LAYERS HERE. */
		      ALOGD_IF(OVERLAY_DEBUG, "%s(%d):Layer %d is OVERLAY", __FUNCTION__, __LINE__, i);
	             /* struct private_handle_t *handle = (struct private_handle_t *) list->hwLayers[i].handle;
	               if(handle && (handle->format == HAL_PIXEL_FORMAT_YCrCb_NV12_VIDEO || handle->format == 0x22) ) {
	                  hwc_overlay_video(ctx, dpy, &list->hwLayers[i]);
	                  video_flag = 1;                  
                      }*/
	              hwc_overlay_ui(ctx, dpy,&list->hwLayers[i],&fb_info.win_par[winid]); 
	              winid++;
		      break;
		    }
		    case HWC_FRAMEBUFFER_TARGET:
                    {
			 #if 0	
			  hwc_enable_display(ctx,&list->hwLayers[i]);
			  if(ctx->dpyAttr[dpy].enabledisplay == false)
			  	return 0;
			 #endif
           #ifdef RK312X
                          if (g_composer_mode!=ALL_TO_LCDC && g_composer_mode!=ONE_TO_LCDC)
                          {
                             hwc_overlay_ui(ctx, dpy,&list->hwLayers[i],&fb_info.win_par[winid]);
                             winid++;
                          }
                          if (g_composer_mode == VIDEO_TO_LCDC && (list->numHwLayers-1)==1) {
     	                      struct private_handle_t *handle = (struct private_handle_t *) list->hwLayers[i].handle;
                              if (handle) {
                                   memset((void*)handle->base,0xFFFFFF00,handle->width*handle->height*4);
                              }
                          }
           #else
                          if (g_composer_mode==VIDEO_TO_LCDC && (list->numHwLayers-1)==1) {
                            break;
                          }

                          if (g_composer_mode!=ALL_TO_LCDC && g_composer_mode!=ONE_TO_LCDC) {
                             hwc_overlay_ui(ctx, dpy,&list->hwLayers[i],&fb_info.win_par[winid]);
                             winid++;
                          }
           #endif
		       break;
	             }
		    default:
		       ALOGD_IF(HWC_DEBUG, "%s(%d):Layer %d is FRAMEBUFFER", __FUNCTION__, __LINE__, i);
		       break;
		    }
		    if (list->hwLayers[i].acquireFenceFd >0)
				  close(list->hwLayers[i].acquireFenceFd);
           }
	/*	 if (video_flag==0) {
            if (video_exit_flag > 0) {
           //   reset_video_info(ctx,0); 
              video_exit_flag = 0;   
            }       
         } else {
           video_exit_flag = 1;
         }*/
	
	/*if(!sprite_flag && (ctx->dpyAttr[dpy].layer[2].active == 1))
	{
		hwc_enable_layer(ctx, dpy, 2, 0);
	}
	if(sprite_flag&& (ctx->dpyAttr[dpy].layer[2].active == 0))
	{
		hwc_enable_layer(ctx, dpy, 2, 1);
	}*/

	/*if(ctx->dpyAttr[dpy].layer[0].active == 1 && overlay_flag && list->numHwLayers-1 == 1)
	{
		//There is only one layer and this layet is overlay to win0.
		//So we disable win1 which is map to fb0
		hwc_enable_layer(ctx, dpy, 0, 0);
	}
	else if(ctx->dpyAttr[dpy].layer[0].active == 0 && (overlay_flag == 0 || list->numHwLayers-1 > 1) )
	{
		hwc_enable_layer(ctx, dpy, 0, 1);
	}
	
	if (!overlay_flag && ctx->dpyAttr[dpy].layer[1].active) {
		// Close video layer
		
		//ALOGD("Close video layer ctx->mcurmode = %s ctx->ismodechange = %d**",ctx->mCurmode,ctx->isModechange);
		hwc_enable_layer(ctx, dpy, 1, 0);
		hwc_set_hdmi_mode(ctx, 0);
	}*/
      if (ctx->isBlank>0){
          return 0;
      }
      dump_list(list);
	 // if(ctx->dpyAttr[dpy].enabledisplay == true){	
	      if (ioctl(ctx->dpyAttr[dpy].layer[0].fd, RK_FBIOSET_CONFIG_DONE, &fb_info) == -1)
	      {
	           ALOGE("RK_FBIOSET_CONFIG_DONE fail,fd=%d",ctx->dpyAttr[dpy].layer[0].fd);
	           return -errno;
	      }	  
	// }
      for (int k=0;k<list->numHwLayers;k++)
      {
          for(int j=0;j<RK_MAX_BUF_NUM;j++)
          {
            if(fb_info.rel_fence_fd[j] != -1)
            {
                if(list->hwLayers[k].compositionType==HWC_OVERLAY)
                {
                    list->hwLayers[k].releaseFenceFd = fb_info.rel_fence_fd[j];
                    //close(fb_info.rel_fence_fd[j]);
                    fb_info.rel_fence_fd[j] = -1;
                    break;
                }     
                else if (list->hwLayers[k].compositionType==HWC_FRAMEBUFFER_TARGET)
                {
                    list->hwLayers[k].releaseFenceFd = fb_info.rel_fence_fd[j];
                    fb_info.rel_fence_fd[j]=-1;
                    break;
                }
                else
                {
                    close(fb_info.rel_fence_fd[j]);
                    fb_info.rel_fence_fd[j]=-1;
                }
             }
          }            
      }
    //}
    if (fb_info.ret_fence_fd > 0)
    {
      list->retireFenceFd = fb_info.ret_fence_fd;
    }

   /* if (NeedSwap) {    	
	EGLBoolean sucess = eglSwapBuffers((EGLDisplay)list->dpy,
	    				(EGLSurface)list->sur);
	if (!sucess) {
	    ALOGE("%s(%d):  eglSwapBuffers Failed", __FUNCTION__, __LINE__);	
	    ret = HWC_EGL_ERROR;
        }
    }*/

	dump_fps();
	return ret;
}

static int hwc_set(hwc_composer_device_1_t *dev,
        size_t numDisplays, hwc_display_contents_1_t** displays)
{
    if (!numDisplays || !displays)
        return 0;

    int ret = 0;
    hwc_context_t* ctx = (hwc_context_t*)(dev);
	
    for (uint32_t i = 0; i < numDisplays; i++) {
        hwc_display_contents_1_t* list = displays[i];
        switch(i) {
            case HWC_DISPLAY_PRIMARY:
                ret = hwc_set_primary(ctx, list);
                break;
//            case HWC_DISPLAY_EXTERNAL:
//                ret = hwc_set_external(ctx, list, i);
//                break;
            case HWC_DISPLAY_VIRTUAL:
                if (list) {
                  ret = hwc_set_virtual(ctx, list);
			    }
                break;
            default:
                ret = -EINVAL;
        }
    }
	
    return ret;
}

static int hwc_event_control(struct hwc_composer_device_1* dev,
        int dpy, int event, int enable)
{
	int ret = 0;
	
    hwc_context_t* ctx = (hwc_context_t*)(dev);
//    if(!ctx->dpyAttr[dpy].isActive) {
//        ALOGE("Display is blanked - Cannot %s vsync",
//              enable ? "enable" : "disable");
//        return -EINVAL;
//    }

    switch(event) {
        case HWC_EVENT_VSYNC:
            if (ctx->vstate.enable == enable)
                break;
            ret = hwc_vsync_control(ctx, dpy, enable);
            if(ret == 0)
                ctx->vstate.enable = !!enable;
            ALOGD_IF(HWC_DEBUG, "VSYNC state changed to %s",
                      (enable)?"ENABLED":"DISABLED");
            break;
        default:
            ret = -EINVAL;
    }
    return ret;
}

static int hwc_blank(struct hwc_composer_device_1 *dev, int dpy, int blank)
{
	// We're using an older method of screen blanking based on
	// early_suspend in the kernel.  No need to do anything here.
	hwc_context_t* ctx = (hwc_context_t*)(dev);
	ALOGD("hwc_blank dpy[%d],blank[%d]",dpy,blank);
	switch (dpy) {
	case HWC_DISPLAY_PRIMARY: {
		int fb_blank = blank ? FB_BLANK_POWERDOWN : FB_BLANK_UNBLANK;
                ctx->isBlank = fb_blank;
		int err = ioctl(ctx->dpyAttr[dpy].layer[0].fd, FBIOBLANK, fb_blank);
		if (err < 0) {
			if (errno == EBUSY)
				ALOGD("%sblank ioctl failed (display already %sblanked)",
						blank ? "" : "un", blank ? "" : "un");
			else
				ALOGE("%sblank ioctl failed: %s", blank ? "" : "un",
						strerror(errno));
			return -errno;
		}
		break;
	}

	case HWC_DISPLAY_EXTERNAL:
		break;

	default:
		return -EINVAL;

	}

	return 0;
}


static int hwc_query(struct hwc_composer_device_1* dev,int what, int* value)
{

    return 0;
}

static void hwc_registerProcs(struct hwc_composer_device_1* dev,
            hwc_procs_t const* procs)
{
	struct hwc_context_t* ctx = (struct hwc_context_t*)dev;
	ctx->procs = procs;
	
	// Now that we have the functions needed, kick off
    // the uevent & vsync threads
    init_uevent_thread(ctx);
    init_vsync_thread(ctx);
}

static int hwc_getDisplayConfigs(struct hwc_composer_device_1* dev, int disp,
        uint32_t* configs, size_t* numConfigs) {
    int ret = 0;
    hwc_context_t* ctx = (hwc_context_t*)(dev);
    //in 1.1 there is no way to choose a config, report as config id # 0
    //This config is passed to getDisplayAttributes. Ignore for now.
    switch(disp) {
        case HWC_DISPLAY_PRIMARY:
            if(*numConfigs > 0) {
                configs[0] = 0;
                *numConfigs = 1;
            }
            ret = 0; //NO_ERROR
            break;
        case HWC_DISPLAY_EXTERNAL:
            ret = -1; //Not connected
            if(ctx->dpyAttr[HWC_DISPLAY_EXTERNAL].connected) {
                ret = 0; //NO_ERROR
                if(*numConfigs > 0) {
                    configs[0] = 0;
                    *numConfigs = 1;
                }
            }
            break;
    }
    return ret;
}

static int hwc_getDisplayAttributes(struct hwc_composer_device_1* dev, int disp,
        uint32_t config, const uint32_t* attributes, int32_t* values) {

    hwc_context_t* ctx = (hwc_context_t*)(dev);
    //If hotpluggable displays are inactive return error
    if(disp == HWC_DISPLAY_EXTERNAL && !ctx->dpyAttr[disp].connected) {
        return -1;
    }

    //From HWComposer
    static const uint32_t DISPLAY_ATTRIBUTES[] = {
        HWC_DISPLAY_VSYNC_PERIOD,
        HWC_DISPLAY_WIDTH,
        HWC_DISPLAY_HEIGHT,
        HWC_DISPLAY_DPI_X,
        HWC_DISPLAY_DPI_Y,
        HWC_DISPLAY_NO_ATTRIBUTE,
    };

    const int NUM_DISPLAY_ATTRIBUTES = (sizeof(DISPLAY_ATTRIBUTES) /
            sizeof(DISPLAY_ATTRIBUTES)[0]);

    for (size_t i = 0; i < NUM_DISPLAY_ATTRIBUTES - 1; i++) {
        switch (attributes[i]) {
        case HWC_DISPLAY_VSYNC_PERIOD:
            values[i] = ctx->dpyAttr[disp].vsync_period;
            ALOGD("%s disp = %d, vsync_period = %d",__FUNCTION__, disp,
                    ctx->dpyAttr[disp].vsync_period);
            break;
        case HWC_DISPLAY_WIDTH:
            values[i] = ctx->dpyAttr[disp].xres;
            ALOGD("%s disp = %d, width = %d",__FUNCTION__, disp,
                    ctx->dpyAttr[disp].xres);
            break;
        case HWC_DISPLAY_HEIGHT:
            values[i] = ctx->dpyAttr[disp].yres;
            ALOGD("%s disp = %d, height = %d",__FUNCTION__, disp,
                    ctx->dpyAttr[disp].yres);
            break;
        case HWC_DISPLAY_DPI_X:
            values[i] = (int32_t) (ctx->dpyAttr[disp].xdpi*1000.0);
            ALOGD("%s disp = %d, xdpi = %f",__FUNCTION__, disp,
                    ctx->dpyAttr[disp].xdpi);
            break;
        case HWC_DISPLAY_DPI_Y:
            values[i] = (int32_t) (ctx->dpyAttr[disp].ydpi*1000.0);
            ALOGD("%s disp = %d, ydpi = %f",__FUNCTION__, disp,
                    ctx->dpyAttr[disp].ydpi);
            break;
        default:
            ALOGE("Unknown display attribute %d",
                    attributes[i]);
            return -EINVAL;
        }
    }
    return 0;
}

static void hwc_dump(struct hwc_composer_device_1* dev, char *buff, int buff_len)
{

}

static int hwc_device_close(struct hw_device_t *dev)
{
    struct hwc_context_t* ctx = (struct hwc_context_t*)dev;
    if (ctx) {
    	if(ctx->mCopyBit) {
    		delete ctx->mCopyBit;
    		ctx->mCopyBit = NULL;
        }
        if(ctx->dpyAttr[0].layer[1].active)
        	hwc_enable_layer(ctx, 0, 1, 0);
		if(ctx->dpyAttr[0].layer[2].active)
        	hwc_enable_layer(ctx, 0, 2, 0);
        free(ctx);
        ctx = NULL;
    }
    return 0;
}

/*****************************************************************************/
static int hwc_device_open(const struct hw_module_t* module, const char* name,
        struct hw_device_t** device)
{
    int ret = 0;
	
    if (strcmp(name, HWC_HARDWARE_COMPOSER))
    	return -EINVAL;

    struct hwc_context_t *dev;
    dev = (hwc_context_t*)malloc(sizeof(*dev));

    /* initialize our state here */
    memset(dev, 0, sizeof(*dev));
	//Initialize hwc context
    ret = openFramebufferDevice(dev);
    if(ret)
    	return ret;
    
    /* initialize the procs */
    dev->device.common.tag = HARDWARE_DEVICE_TAG;
    dev->device.common.version = HWC_DEVICE_API_VERSION_1_3;
    dev->device.common.module = const_cast<hw_module_t*>(module);
    dev->device.common.close = hwc_device_close;

    dev->device.prepare = hwc_prepare;
    dev->device.set = hwc_set;
	  dev->device.eventControl = hwc_event_control;
	  dev->device.blank = hwc_blank;
	  dev->device.query = hwc_query;
    dev->device.dump = hwc_dump;
	  dev->device.registerProcs = hwc_registerProcs;
	  dev->device.getDisplayConfigs = hwc_getDisplayConfigs;
    dev->device.getDisplayAttributes = hwc_getDisplayAttributes;
    *device = &dev->device.common;
	ALOGI("hwc box version open success.");
	return 0;
}
