#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <sys/stat.h>

int help(void)
{
	printf("=====================================================================\n");
	printf("Rockchip HDCP 2.x key section tool, version 1.0. by Thomas Lai.\n");
	printf("=====================================================================\n");
	printf("rk_hecp20key <key file> [status|get number]\n");
	
	printf("  key file: the file contains HDCP 2.x keys.\n");
	printf("  status  : show you the current usage about HDCP 2.x keys.\n");
	printf("  get     : apply the <number> key to a new file for manufacturing.\n");
	printf("=====================================================================\n");
	
	return 0;
}

int current = 0;

int get_status(char *file)
{
	char log_file[256], line[256], *p = NULL;
	FILE *fp = NULL;
	
	sprintf(log_file, "%s.log", file);
	printf("The log file: <%s>\n", log_file);
	
	fp = fopen(log_file, "r");
	if (fp == NULL)
	{
		printf("The log file is NULL.\n");
		current = 0;
		return 0;
	}
	
	// CURRENT=888
	if (fgets(line, 256, fp) == NULL)
	{
		fclose(fp);
		printf("Invalid LOG file format.\n");
		return -1;
	}
	
	if (strstr(line, "CURRENT") == NULL)
	{
		fclose(fp);
		printf("Invalid LOG field format.\n");
		return -1;
	}
	
	p = line + 8;
	current = atoi(p);
	
	fclose(fp);
	
	return 0;
}

#define KEY_LEN 862

int file_key_num = 0;

int analyse_key_file(char *file)
{
	int fd = -1, len, padding = 40;
	unsigned char data[1024];
	
	fd = open(file, O_RDONLY);
	if (fd < 0)
	{
		printf("Open key file <%s> fail.\n", file);
		return -1;
	}
	
	// Ignore the header.
	len = read(fd, data, padding);
	if (len != padding)
	{
		printf("Read padding fail: len=%d\n", len);
		close(fd);
		return -1;
	}
	
	while (1)
	{
		len = read(fd, data, KEY_LEN);
		//printf("len = %d\n", len);
		if (len == KEY_LEN)
			file_key_num++;
		else
			break;
	}
	
	close(fd);
	
	return 0;
}

int copy_key_file(char *file, int num)
{
	int ret = 0, len, i, wlen;
	int orig_fd = -1, new_fd = -1, padding = 40;
	char new_file[128];
	unsigned char data[1024];
	
	orig_fd = open(file, O_RDONLY);
	if (orig_fd < 0)
	{
		printf("Open key file <%s> fail.\n", file);
		return -1;
	}
	
	sprintf(new_file, "%s-s%d-n%d", file, current, num);
	new_fd = open(new_file, O_WRONLY | O_CREAT);
	if (new_fd < 0)
	{
		printf("Open key file <%s> fail.\n", new_file);
		close(orig_fd);
		return -1;
	}
	
	// write the header in key file. 40 bytes.
	len = read(orig_fd, data, padding);
	if (len != padding)
	{
		printf("Read padding fail: len=%d\n", len);
		close(new_fd);
		close(orig_fd);
		return -1;
	}
	wlen = write(new_fd, data, padding);
	if (wlen != padding)
		printf("Warning: write key header fail\n");
	
	
	// Skip the keys used already.
	for (i = 0; i < current; i++)
	{
		len = read(orig_fd, data, KEY_LEN);
		//printf("len = %d\n", len);
		if (len != KEY_LEN)
			break;
	}
	if (i != current)
	{
		printf("Skipping used keys fail. i=%d current=%d\n", i, current);
		close(new_fd);
		close(orig_fd);
		return -1;
	}
	
	// Copy the keys to new file really.
	for (i = 0; i < num; i++)
	{
		len = read(orig_fd, data, KEY_LEN);
		//printf("len = %d\n", len);
		if (len == KEY_LEN)
		{
			wlen = write(new_fd, data, KEY_LEN);
			if (wlen != KEY_LEN)
				printf("Warning: write key data fail: KEY_LEN\n", wlen);
		}
		else
			break;
	}
	if (i != num)
	{
		printf("Coping new keys fail. i=%d current=%d\n", i, num);
		close(new_fd);
		close(orig_fd);
		return -1;
	}
	
	close(new_fd);
	close(orig_fd);
	
	return num;
}

int write_log(char *file)
{
	char log_file[256], line[256], *p = NULL;
	FILE *fp = NULL;
	
	sprintf(log_file, "%s.log", file);
	printf("The log file: <%s>\n", log_file);
	
	fp = fopen(log_file, "w");
	if (fp == NULL)
	{
		printf("The log file is NULL.\n");
		current = 0;
		return 0;
	}
	
	fprintf(fp, "CURRENT=%d\n", current);
	
	fclose(fp);
	
	return 0;
}

int main(int argc, char **argv)
{
	int ret, get_num;
	char new_file[128];
	
	//if (argc < 2)
	//	return help();
	help();
	
	// Analyse key file.
	ret = analyse_key_file(argv[1]);
	if (ret == -1)
	{
		printf("Analyse key file error.\n");
		return -1;
	}
	printf("There are <%d> keys in file <%s>.\n", file_key_num, argv[1]);
	
	/* Show the current status about keys in key file. */
	ret = get_status(argv[1]);
	if (ret == -1)
	{
		printf("Get status error.\n");
		return -1;
	}
	
	printf("Current No.: %d\n", current);
	
	// Get <number> keys to new file.
	if (argc < 4)
		return 0;
	
	if (memcmp(argv[2], "get", 3) != 0)
		return 0;
	
	get_num = atoi(argv[3]);
	printf("You want to fetch <%d> keys.\n", get_num);
	
	if ((current + get_num) > file_key_num)
	{
		printf("You wanna too much! (Only <%d> keys are valid.)\n", file_key_num - current);
		return 0;
	}
	
	// Copy the data from original key file.
	sprintf(new_file, "%s-s%d-n%d", argv[1], current, get_num);
	ret = copy_key_file(argv[1], get_num);
	if (ret <= 0)
	{
		printf("Copy keys fail. ret=%d\n", ret);
		return 0;
	}
	printf("Copy the <%d> keys to file <%s>\n", get_num, new_file);
	
	// Write the log
	current += get_num;
	write_log(argv[1]);
	
	return 0;
}
