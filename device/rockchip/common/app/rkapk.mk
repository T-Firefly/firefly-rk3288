CUR_PATH := device/rockchip/common/app
PRODUCT_PACKAGES += \
		RkApkinstaller \
		RkExplorer \
		RkBoxVideoPlayer \
		StressTest \
		RkMusic \
		eHomeMediaCenter_box \
		WifiDisplay \
		RKGameControlSettingV1.0.1\
		RKSettings \
		RKBasicSettings \
		HDMINotification
